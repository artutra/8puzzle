import java.util.ArrayList;
import java.util.Arrays;

public class Largura {
	int nivelMax = 0;
	ArrayList<Tabuleiro> visitados = new ArrayList<Tabuleiro>();
	ArrayList<Tabuleiro> fronteira = new ArrayList<Tabuleiro>();

	public Tabuleiro buscaLargura(Tabuleiro no) {
		Tabuleiro res = no;
		if (!no.isResultado()) {
			no.constroiFilhos();
			for (Tabuleiro tabuleiro : no.filhos) {
				fronteira.add(tabuleiro);
			}
			if(no.nivel>nivelMax)
				nivelMax = no.nivel;
			visitados.add(no);
			while (!res.isResultado()) {
				res = buscaNaFronteira();

			}
		} else {
			return no;
		}
		return res;
	}

	private Tabuleiro buscaNaFronteira() {
		Tabuleiro res=null;
		ArrayList<Tabuleiro> fronteira2 = new ArrayList<Tabuleiro>();
		for(Tabuleiro tabuleiro: fronteira){
			if(!foiVisitado(tabuleiro)){
			if(!tabuleiro.isResultado()){
				res=tabuleiro;
				tabuleiro.constroiFilhos();
				
				for(Tabuleiro tabuleiro2: tabuleiro.filhos){
					fronteira2.add(tabuleiro2);
				}
				visitados.add(tabuleiro);
			}else{
			return tabuleiro;
			}
			}
		}
		for(Tabuleiro tabuleiro:fronteira2){
			fronteira.add(tabuleiro);
		}
		//fronteira.remove(res);
		return res;
	}

	public boolean foiVisitado(Tabuleiro tab) {
		for (Tabuleiro tabuleiro : visitados) {
			if (Arrays.deepEquals(tabuleiro.matriz, tab.matriz)) {
				return true;
			}
		}
		return false;
	}

}
