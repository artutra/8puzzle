import java.util.ArrayList;


public class Tabuleiro {
	int nivel;
	int somaPeso;
	
	Tabuleiro pai = null;
	ArrayList<Tabuleiro> filhos = new ArrayList<Tabuleiro>();
	int[][] matriz = null;
	
	
	public Tabuleiro(int[][] matriz, int nivel, Tabuleiro pai, int somaPeso){
		this.nivel = nivel+1;
		this.matriz = matriz;
		this.pai = pai;
		this.somaPeso = somaPeso;
	}
	
	public Tabuleiro(int[][] matriz, int nivel, Tabuleiro pai){
		this.nivel = nivel+1;
		this.matriz = matriz;
		this.pai = pai;
	}
	
	public Tabuleiro(int[][] matriz, int nivel){
		this.nivel = nivel+1;
		this.matriz = matriz;
	}
	
	public void constroiFilhos(){
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				if(matriz[i][j]==0){
					//Direita
					if(j<matriz.length-1)
						filhos.add(movePeca(i,j,i,j+1));
					//Esquerda
					if(j>0)
						filhos.add(movePeca(i,j,i,j-1));
					//Cima
					if(i>0)
						filhos.add(movePeca(i,j,i-1,j));
					//Baixo
					if(i<matriz.length-1)
						filhos.add(movePeca(i,j,i+1,j));
					return;
				}
			}
		}
	}
	
	public boolean isResultado(){
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				if( i == matriz.length-1 && j == matriz.length-1){
					if(matriz[i][j] != 0)
						return false;
				}else{
					if(matriz[i][j] != i*matriz.length+j+1){
						return false;
					}	
				}				
			}	
		}
		return true;
	}
	
	public Tabuleiro movePeca(int zeroi, int zeroj, int movei, int movej){
		int[][] movimento = new int[matriz.length][matriz.length];
		for (int i = 0; i < movimento.length; i++) {
			for (int j = 0; j < movimento.length; j++) {
				movimento[i][j] = matriz[i][j];
			}
		}
		
		movimento[zeroi][zeroj] = movimento[movei][movej];
		movimento[movei][movej] = 0;
		
		Heuristica h = new Heuristica();
		Tabuleiro tab = new Tabuleiro(movimento, this.nivel, this);
		tab.setSomaA(h.manhattan(this)+h.manhattan(tab));
		return tab;
	}
	public void setSomaA(int somaPeso){
		this.somaPeso = somaPeso;
	}
	public int getNivel(){
		return nivel;
	}
	public ArrayList<Tabuleiro> getFilhos(){
		return filhos;
	}
}
