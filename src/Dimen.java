
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Dimen {

	private JFrame frame;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dimen window = new Dimen();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Dimen() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 136, 158);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JRadioButton rdbtnx = new JRadioButton("3x3");
		rdbtnx.setSelected(true);
		rdbtnx.setBounds(6, 7, 109, 23);
		frame.getContentPane().add(rdbtnx);
		
		JRadioButton rdbtnx_1 = new JRadioButton("4x4");
		rdbtnx_1.setBounds(6, 33, 109, 23);
		frame.getContentPane().add(rdbtnx_1);
		
		JRadioButton rdbtnx_2 = new JRadioButton("5x4");
		rdbtnx_2.setBounds(6, 59, 109, 23);
		frame.getContentPane().add(rdbtnx_2);
		
		JButton btnContinuar = new JButton("Continuar");
		btnContinuar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(rdbtnx.isSelected()){
					Tabuleiro33.main(null);
				}
			}
		});
		btnContinuar.setBounds(16, 89, 89, 23);
		frame.getContentPane().add(btnContinuar);
	}
}
