import java.util.ArrayList;
import java.util.Arrays;

public class Gulosa {
	int nivelMax = 0;
	ArrayList<Tabuleiro> visitados = new ArrayList<Tabuleiro>();
	ArrayList<Tabuleiro> fronteira = new ArrayList<Tabuleiro>();

	public Tabuleiro buscaGulosa(Tabuleiro no) {
		Tabuleiro res = no;
		if (!no.isResultado()) {
			no.constroiFilhos();
			for (Tabuleiro tabuleiro : no.filhos) {
				fronteira.add(tabuleiro);
			}
			if(no.nivel>nivelMax)
				nivelMax = no.nivel;
			visitados.add(no);
			while (!res.isResultado()) {
				res = buscaNaFronteira();

			}
		} else {
			return no;
		}
		return res;
	}

	private Tabuleiro buscaNaFronteira() {
		Tabuleiro res = null, melhor = null;
		int numMelhor = 0;
		for (Tabuleiro tabuleiro : fronteira) {
			if (!foiVisitado(tabuleiro)) {
				if (!tabuleiro.isResultado()) {
					res = tabuleiro;
					if (melhor == null) {
						melhor = tabuleiro;
						numMelhor = manhattan(melhor);
					} else if (manhattan(tabuleiro) < numMelhor) {
						melhor = tabuleiro;
						numMelhor = manhattan(melhor);
					}

				} else {
					return tabuleiro;
				}
			}
		}
		melhor.constroiFilhos();

		for (Tabuleiro tabuleiro : melhor.filhos) {
			fronteira.add(tabuleiro);
		}
		visitados.add(melhor);		
		return res;

	}

	public int manhattan(Tabuleiro tab) {
		int total = 0;
		for (int i = 0; i < tab.matriz.length; i++) {
			for (int j = 0; j < tab.matriz.length; j++) {
				int n = tab.matriz[i][j];
				int ifinal, jfinal;
				if (n != 0) {
					ifinal = (n - 1) / tab.matriz.length;
					jfinal = (n - 1) % tab.matriz.length;
					total += (i - ifinal) + (j - jfinal);
				}
			}
		}
		return total;
	}

	public boolean foiVisitado(Tabuleiro tab) {
		for (Tabuleiro tabuleiro : visitados) {
			if (Arrays.deepEquals(tabuleiro.matriz, tab.matriz)) {
				return true;
			}
		}
		return false;
	}
}
