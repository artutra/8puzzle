import java.util.ArrayList;
import java.util.Arrays;


public class AEstrela {
	int nivelMax = 0;
	Heuristica h = new Heuristica();
	
	ArrayList<Tabuleiro> visitados = new ArrayList<Tabuleiro>();
	
	ArrayList<Tabuleiro> listA = new ArrayList<Tabuleiro>();
	
	public Tabuleiro buscaAEstrela(Tabuleiro raiz){
		iniciaListA(raiz);
		Tabuleiro res = null;
		do {
			res = buscaAEstrela();
		} while (res==null);
		return res;
	}
	
	public Tabuleiro buscaAEstrela(){
		System.out.println(listA.size());
		Tabuleiro no = melhorEscolha(listA);
		if(no.isResultado())
			return no;
		else{
			if(no.nivel>nivelMax)
				nivelMax = no.nivel;
			visitados.add(no);
			listA.remove(no);
			no.constroiFilhos();
			for (Tabuleiro filho : no.filhos) {
				//Se filho ainda nao foi visitado
				if(!foiVisitado(filho)){
					listA.add(0, filho);					
				}
			}
		}
		return null;
	}
	
	public Tabuleiro melhorEscolha(ArrayList<Tabuleiro> list){
		Tabuleiro melhor = null;
		for (Tabuleiro tabuleiro : list) {
			if(melhor == null)
				melhor = tabuleiro;
			else{
				if(tabuleiro.somaPeso<melhor.somaPeso)
					melhor = tabuleiro;
			}
		}
		return melhor;
	}
	
	public void iniciaListA(Tabuleiro raiz){
		listA.add(raiz);
	}
	
	public boolean foiVisitado(Tabuleiro tab){
		for (Tabuleiro tabuleiro : visitados) {
			if(Arrays.deepEquals(tabuleiro.matriz, tab.matriz)){
				return true;
			}
		}
		return false;
	}
}
