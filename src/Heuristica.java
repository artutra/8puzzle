
public class Heuristica {
	public int manhattan(Tabuleiro tab){
		int total = 0;
		for (int i = 0; i < tab.matriz.length; i++) {
			for (int j = 0; j < tab.matriz.length; j++) {
				int n = tab.matriz[i][j];
				int ifinal,jfinal;
				if(n != 0){
					ifinal = (n-1)/tab.matriz.length;
					jfinal = (n-1)%tab.matriz.length;
					total+=Math.abs(i-ifinal)+Math.abs(j-jfinal);
				}
			}
		}
		return total;
	}
}