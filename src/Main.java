
public class Main {

	public static void main(int[][] matriz, int algo) {
		// int[][] matriz = {{8,3,5},{0,6,7},{2,4,1}};
		// int[][] matriz = {{2,3,8},{7,0,4},{5,6,1}};
		// int[][] matriz = {{0,1,3},{5,2,6},{4,7,8}};
		Tabuleiro no = new Tabuleiro(matriz, 0, null, 0);
		Tabuleiro res2=null;
		if (algo == 1) {
			Profundidade busca = new Profundidade();
			res2 = busca.buscaProfundidade(no);
		}
		if (algo == 2) {
			Largura busca2 = new Largura();
			res2 = busca2.buscaLargura(no);
		}
		if (algo == 3) {
			Gulosa busca2 = new Gulosa();
			res2 = busca2.buscaGulosa(no);
		}
		if (algo == 4) {
			AEstrela busca3 = new AEstrela();
			res2 = busca3.buscaAEstrela(no);
		}
		
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				System.out.print(res2.matriz[i][j]);
			}
			System.out.println("");
		}
		System.out.println(res2.nivel);

	}

	public static void imprimeMatriz(int[][] matriz){
		System.out.println("----------");
		for (int i = 0; i < matriz.length; i++) {
			for (int j = 0; j < matriz.length; j++) {
				System.out.print(matriz[i][j]);
			}
			System.out.println("");
		}
		System.out.println("----------");
	}
}
