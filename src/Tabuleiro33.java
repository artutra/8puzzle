
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter.DEFAULT; 
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JTextField;
import javax.swing.JRadioButton;
import javax.swing.JButton;
import javax.swing.JTextArea;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class Tabuleiro33 {

	private JFrame frame;
	private JRadioButton rdbtnProfundidade;
	private JRadioButton rdbtnLargura;
	private JRadioButton rdbtnA;
	private JButton btnNewButton;
	/**
	 * @wbp.nonvisual location=-38,39
	 */
	private final JTextArea textArea = new JTextArea();
	private JRadioButton radioButton;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private JTextField textField_8;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Tabuleiro33 window = new Tabuleiro33();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public Tabuleiro33() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 270, 194);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		rdbtnProfundidade = new JRadioButton("Profundidade");
		rdbtnProfundidade.setSelected(true);
		rdbtnProfundidade.setBounds(143, 11, 109, 23);
		frame.getContentPane().add(rdbtnProfundidade);
		
		rdbtnLargura = new JRadioButton("Largura");
		rdbtnLargura.setBounds(143, 31, 109, 23);
		frame.getContentPane().add(rdbtnLargura);
		
		rdbtnA = new JRadioButton("Gulosa");
		rdbtnA.setBounds(143, 50, 109, 23);
		frame.getContentPane().add(rdbtnA);
		
		btnNewButton = new JButton("Continuar");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				int[][] matriz=new int[3][3];
				matriz[0][0]=Integer.parseInt(textField.getText());
				matriz[0][1]=Integer.parseInt(textField_1.getText());
				matriz[0][2]=Integer.parseInt(textField_2.getText());
				matriz[1][0]=Integer.parseInt(textField_3.getText());
				matriz[1][1]=Integer.parseInt(textField_4.getText());
				matriz[1][2]=Integer.parseInt(textField_5.getText());
				matriz[2][0]=Integer.parseInt(textField_6.getText());
				matriz[2][1]=Integer.parseInt(textField_7.getText());
				matriz[2][2]=Integer.parseInt(textField_8.getText());
				if(rdbtnProfundidade.isSelected()){
					Main.main(matriz,1);
				}
				if(rdbtnLargura.isSelected()){
					Main.main(matriz,2);
				}
				if(rdbtnA.isSelected()){
					Main.main(matriz,3);
				}
				if(radioButton.isSelected()){
					Main.main(matriz,4);
				}
			}
		});
		btnNewButton.setBounds(82, 119, 89, 23);
		frame.getContentPane().add(btnNewButton);
		
		radioButton = new JRadioButton("A*");
		radioButton.setBounds(143, 72, 109, 23);
		frame.getContentPane().add(radioButton);
		
		textField = new JTextField();
		textField.setText("1");
		textField.setBounds(10, 11, 26, 20);
		frame.getContentPane().add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setText("2");
		textField_1.setColumns(10);
		textField_1.setBounds(46, 11, 26, 20);
		frame.getContentPane().add(textField_1);
		
		textField_2 = new JTextField();
		textField_2.setText("3");
		textField_2.setColumns(10);
		textField_2.setBounds(82, 11, 26, 20);
		frame.getContentPane().add(textField_2);
		
		textField_3 = new JTextField();
		textField_3.setText("4");
		textField_3.setColumns(10);
		textField_3.setBounds(10, 49, 26, 20);
		frame.getContentPane().add(textField_3);
		
		textField_4 = new JTextField();
		textField_4.setText("5");
		textField_4.setColumns(10);
		textField_4.setBounds(46, 49, 26, 20);
		frame.getContentPane().add(textField_4);
		
		textField_5 = new JTextField();
		textField_5.setText("6");
		textField_5.setColumns(10);
		textField_5.setBounds(82, 49, 26, 20);
		frame.getContentPane().add(textField_5);
		
		textField_6 = new JTextField();
		textField_6.setText("7");
		textField_6.setColumns(10);
		textField_6.setBounds(10, 88, 26, 20);
		frame.getContentPane().add(textField_6);
		
		textField_7 = new JTextField();
		textField_7.setText("8");
		textField_7.setColumns(10);
		textField_7.setBounds(46, 88, 26, 20);
		frame.getContentPane().add(textField_7);
		
		textField_8 = new JTextField();
		textField_8.setText("0");
		textField_8.setColumns(10);
		textField_8.setBounds(82, 88, 26, 20);
		frame.getContentPane().add(textField_8);
	}
}
