import java.util.ArrayList;
import java.util.Arrays;


public class Profundidade {
	int nivelMax = 0;
	ArrayList<Tabuleiro> visitados = new ArrayList<Tabuleiro>();
	
	ArrayList<Tabuleiro> listProf = new ArrayList<Tabuleiro>();
	
	public Tabuleiro buscaProfundidade(Tabuleiro raiz){
		iniciaListProf(raiz);
		Tabuleiro res = null;
		do {
			res = buscaProfundidade();
		} while (res==null);
		return res;
	}
	
	public Tabuleiro buscaProfundidade(){
		System.out.println(listProf.size());
		Tabuleiro no = listProf.get(0);
		if(no.isResultado())
			return no;
		else{
			if(no.nivel>nivelMax)
				nivelMax = no.nivel;
			visitados.add(no);
			listProf.remove(no);
			no.constroiFilhos();
			for (Tabuleiro filho : no.filhos) {
				//Se filho ainda nao foi visitado
				if(!foiVisitado(filho)){
					listProf.add(0, filho);					
				}
			}
		}
		return null;
	}
	public void iniciaListProf(Tabuleiro raiz){
		listProf.add(raiz);
	}
	
	public boolean foiVisitado(Tabuleiro tab){
		for (Tabuleiro tabuleiro : visitados) {
			if(Arrays.deepEquals(tabuleiro.matriz, tab.matriz)){
				return true;
			}
		}
		return false;
	}
}
